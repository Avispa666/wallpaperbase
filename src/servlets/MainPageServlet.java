package servlets;

import DB.CategoryDAO;
import DB.TagDAO;
import DB.WallpaperDAO;
import Freemarker.FreemarkerConfig;
import antities.Category;
import antities.User;
import antities.Wallpaper;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by avispa on 17/10/2017.
 */
@WebServlet(name = "servlets.MainPageServlet")
public class MainPageServlet extends HttpServlet {
    private static final int LIMIT = 20;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> root = new HashMap<>();

        boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
        int lastId = 0;
        if (request.getParameter("lastId") != null) {
            try {
                lastId = Integer.parseInt(request.getParameter("lastId"));
            } catch (NumberFormatException e) {
//                e.printStackTrace();
            }
        }
        List<Category> categories = null;
        List<Wallpaper> wallpapers = null;
        Boolean all;
        if (request.getParameter("query") != null && request.getParameter("all") != null) {
            String query = request.getParameter("query");
            if (!checkInput(query)) return;
            all = Boolean.parseBoolean(request.getParameter("all"));
            System.out.println("query: " + query + " all: " + all);
            if (all) wallpapers = TagDAO.searchTags(query, lastId, LIMIT);
            else categories = CategoryDAO.searchInCategories(query.toLowerCase(), lastId, LIMIT);
        } else {
            categories = CategoryDAO.getCategories(lastId, LIMIT);
            wallpapers = WallpaperDAO.getWallpapers(lastId, LIMIT);
            all = false;
        }
        if (ajax) {
            Gson gson = new Gson();
            String json;
            if (all) json = gson.toJson(Wallpaper.prepareList(wallpapers));
            else json = gson.toJson(Category.prepareList(categories));
            System.out.println(json);
//            json = "[" + json + "]";
            response.setContentType("application/json");
            response.getWriter().write(json);
            return;
        }
        User u = (User) request.getSession().getAttribute("current_user");
        root.put("user", u);
        root.put("categories", Category.prepareList(categories));
        FreemarkerConfig.process(this, root, response);
    }

    private boolean checkInput(String input) {
        return input.length() <= 50;
    }
}
