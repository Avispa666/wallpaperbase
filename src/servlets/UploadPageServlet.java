package servlets;

import DB.DBHelper;
import DB.WallpaperDAO;
import Freemarker.FreemarkerConfig;
import antities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by avispa on 20/10/2017.
 */
@WebServlet(name = "UploadPageServlet")
@MultipartConfig(fileSizeThreshold=1024*1024*10,
        maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)
public class UploadPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("current_user");
        if (user == null) {
            response.sendRedirect("/");
            return;
        }
        if (request.getParameter("filename") != null) {
            System.out.println("uploading wallpaper");
            String filename = request.getParameter("filename");
            String category = request.getParameter("category");
            String tags = request.getParameter("tags");
            String ext = filename.substring(filename.lastIndexOf('.'));
            Part filePart = request.getPart("wallpaper");
            if (!checkExt(ext)) {
                response.sendRedirect("/profile");
                return;
            }
            File uploads = new File(DBHelper.WAR_PATH + "\\wp_unmoderated"); //TODO hardcoded path
            File file = File.createTempFile("wallpaper", ext, uploads);
            InputStream input = filePart.getInputStream();
            Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            //DBHelper.addUnmoderated(user.getId(), category, tags, file.getName());
            WallpaperDAO.addUnmoderated(user.getId(), category, tags, file.getName());
            input.close();
            response.sendRedirect("/upload");
            return;
        }
    }

    private boolean checkExt(String ext) {
        return ext != null && (ext.equals(".jpg") || ext.equals(".jpeg") || ext.equals(".png"));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> root = new HashMap<>();
        root.put("user", request.getSession().getAttribute("current_user"));
        FreemarkerConfig.process(this, root, response);
    }
}
