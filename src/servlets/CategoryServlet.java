package servlets;

import DB.TagDAO;
import DB.WallpaperDAO;
import Freemarker.FreemarkerConfig;
import antities.Wallpaper;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by avispa on 17/10/2017.
 */
@WebServlet(name = "CategoryServlet")
public class CategoryServlet extends HttpServlet {
    private static final int LIMIT = 20;
    private static final String pattern = ".*\\/categories\\/(\\w+)$";
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Matcher m = Pattern.compile(pattern).matcher(request.getRequestURI());
        List<Wallpaper> wallpapers;
        boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
        if (m.find()) {
            int lastId = 0;
            if (request.getParameter("lastId") != null) {
                try {
                    lastId = Integer.parseInt(request.getParameter("lastId"));
                } catch (NumberFormatException e) {
//                e.printStackTrace();
                }
            }
            System.out.println("category:" + m.group(1));
            if (request.getParameter("query") != null) {
                String query = request.getParameter("query");
                int id = Integer.parseInt(request.getParameter("lastId"));
                wallpapers = TagDAO.searchTagsInCategory(m.group(1), query, id, LIMIT);
            } else {
                wallpapers = WallpaperDAO.getWallpapersByCategory(m.group(1).toLowerCase(), lastId, LIMIT);
            }
            if (ajax) {
                Gson gson = new Gson();
                String json = gson.toJson(Wallpaper.prepareList(wallpapers));
                System.out.println(json);
                response.setContentType("application/json");
                response.getWriter().write(json);
                return;
            }
        }  else {
            response.sendRedirect("/"); //TODO redirect 404
            return;
        }
        if (wallpapers == null || wallpapers.isEmpty()) {
            response.sendRedirect("/"); // should be 404
            return;
        }
        Map<String, Object> root = new HashMap<>();
        root.put("category", wallpapers.get(0).getCategory().getName());
        root.put("wallpapers", Wallpaper.prepareList(wallpapers));
        root.put("user", request.getSession().getAttribute("current_user"));
        FreemarkerConfig.process(this, root, response);
    }
}
