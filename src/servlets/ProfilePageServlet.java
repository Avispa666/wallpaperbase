package servlets;

import DB.DBHelper;
import DB.UserDAO;
import Freemarker.FreemarkerConfig;
import antities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by avispa on 17/10/2017.
 */
@WebServlet(name = "servlets.ProfilePageServlet")
@MultipartConfig(fileSizeThreshold=1024*1024*5,
        maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)
public class ProfilePageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("profile do post");
        String action = request.getParameter("action");
        System.out.println("username_check: " + request.getParameter("username_check"));
        User user = (User) request.getSession().getAttribute("current_user");
        if (user == null) {
            response.sendRedirect("/");
            return;
        }
        if (request.getParameter("username_check") != null) {
            String username = request.getParameter("username_check");
            if (username.equals("")) return;
            System.out.println("request for checking username:" + username + " handled");
            boolean check = UserDAO.isUsernameFree(username);
            if (check) {
                String old_username = ((User) request.getSession().getAttribute("current_user"))
                        .getUsername();
                UserDAO.changeUsername(
                        old_username, username);
                ((User)request.getSession()
                        .getAttribute("current_user")).setUsername(username);
            }
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(check + "");
            return;
        }
        if (request.getParameter("password_set") != null) {
            String[] t = request.getParameter("password_set").split(" ");
            String username = ((User) request.getSession().getAttribute("current_user"))
                    .getUsername();
            UserDAO.changePassword(username, t[0], t[1]);
            return;
        }
        if (request.getParameter("filename") != null) {
            System.out.println("changing profile img");
            String filename = request.getParameter("filename");
            String ext = filename.substring(filename.lastIndexOf('.') + 1);
            if (!checkExt(ext)) return;
            Part filePart = request.getPart("user-photo"); // Retrieves <input type="file" name="file">
            File uploads = new File(DBHelper.WAR_PATH + "\\profile_img"); //TODO hardcoded path
            File file = new File(uploads, user.getId() + "." + ext);
            if (!file.exists()) file.createNewFile();
            InputStream input = filePart.getInputStream();
            Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            UserDAO.changeProfileImg(user, ext);
            user.setImgUrl("/profile_img/" + file.getName());
            input.close();
            response.sendRedirect("/profile");
            return;
        }
        /*
        switch (action) {
            case "change_username":
                String old_username = ((User) request.getSession().getAttribute("current_user"))
                        .getUsername();
                DBHelper.changeUsername(
                        old_username, request.getParameter("new_username"));
                ((User)request.getSession()
                        .getAttribute("current_user")).setUsername(request.getParameter("new_username"));
                break;
            case "change_password":
                DBHelper.changePassword(((User)request.getSession().getAttribute("current_user")).getUsername(),
                        request.getParameter("old_password"), request.getParameter("new_password"));
                break;
            default:
                response.sendRedirect("/");
                return;
        }
        response.sendRedirect("/profile");
        */
    }

    private boolean checkExt(String ext) {
        return ext != null && (ext.equals(".jpg") || ext.equals(".jpeg") || ext.equals(".png"));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String,Object> root = new HashMap<>();
        User u = (User) request.getSession().getAttribute("current_user");
        root.put("user", u);
        FreemarkerConfig.process(this, root, response);
    }
}
