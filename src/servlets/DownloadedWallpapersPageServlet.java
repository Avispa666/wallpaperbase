package servlets;

import DB.WallpaperDAO;
import Freemarker.FreemarkerConfig;
import antities.User;
import antities.Wallpaper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by avispa on 2/11/2017.
 */
@WebServlet(name = "DownloadedWallpapersPageServlet")
public class DownloadedWallpapersPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> root = new HashMap<>();
        List<Wallpaper> wallpapers = WallpaperDAO.getLoadedWallpapers(
                (User) request.getSession().getAttribute("current_user"), "downloaded", 0, 20);
        root.put("user", request.getSession().getAttribute("current_user"));
        root.put("wallpapers", wallpapers);
        FreemarkerConfig.process(this, root, response);
    }
}
