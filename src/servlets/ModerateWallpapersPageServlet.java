package servlets;

import DB.WallpaperDAO;
import Freemarker.FreemarkerConfig;
import antities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by avispa on 9/11/2017.
 */
@WebServlet(name = "ModerateWallpapersPageServlet")
public class ModerateWallpapersPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = ((User) request.getSession().getAttribute("current_user"));
//        if (user == null || !user.isAdmin()) {
//            response.sendRedirect("/");
//            return;
//        }
        if (request.getParameter("decision") != null) {
            String decision = request.getParameter("decision");
            int id = Integer.parseInt(request.getParameter("id"));
            String tags = request.getParameter("tags");
            String cat = request.getParameter("category");
            switch (decision) {
                case "submit":
                    response.setContentType("text");
                    break;
                case "decline":
                    WallpaperDAO.declineWallpaper(id);
                    break;
                default:
                    System.out.println("default case");
            }
            response.setContentType("text");
            response.getWriter().write("success");
            return;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = ((User) request.getSession().getAttribute("current_user"));
        if (user == null || !user.isAdmin()) {
            response.sendRedirect("/");
            return;
        }
        Map<String,Object> root = new HashMap<>();
        root.put("user", user);
        root.put("wallpapers", WallpaperDAO.getUnmoderatedWallpapers());
        FreemarkerConfig.process(this, root, response);
    }
}
