package servlets;

import DB.UserDAO;
import Freemarker.FreemarkerConfig;
import antities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by avispa on 17/10/2017.
 */
@WebServlet(name = "servlets.LoginPageServlet")
public class LoginPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        boolean rememberMe = request.getParameter("remember_me") != null;
        System.out.println("remember_me:" + rememberMe);
        if (!checkInput(username, password)) {
            response.sendRedirect("/login");
            return;
        }
        User user = UserDAO.login(username, password);
        if (user != null) {
            request.getSession().setAttribute("current_user", user);
            //TODO  go back after redirect
            if (rememberMe) {
                Cookie cookie = new Cookie("remember_me",
                        URLEncoder.encode(UserDAO.rememberMe(username, password), "UTF-8"));
                cookie.setMaxAge(60*60*24*365); //1 year
                cookie.setSecure(false);
                response.addCookie(cookie);
                System.out.println("cookie added");
            }
            response.sendRedirect("/");
        } else {
            response.sendRedirect("/login");
            request.setAttribute("error", "incorrect username or password"); // Set error msg for ${error}
//            request.getRequestDispatcher("/username").forward(request, response); // Go back to username page.
        }
    }

    private boolean checkInput(String username, String password) {
        return  !Objects.equals(username, "") && !Objects.equals(password, "")
                && username != null && password != null
                && username.length() <= 20 && password.length() <= 25; //TODO check by regex
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> root = new HashMap<>();
        FreemarkerConfig.process(this, root, response);
    }
}
