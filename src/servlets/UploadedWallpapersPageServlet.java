package servlets;

import DB.TagDAO;
import DB.WallpaperDAO;
import Freemarker.FreemarkerConfig;
import antities.User;
import antities.Wallpaper;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by avispa on 2/11/2017.
 */
@WebServlet(name = "UploadedWallpapersPageServlet")
public class UploadedWallpapersPageServlet extends HttpServlet {
    private static final int LIMIT = 20;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> root = new HashMap<>();
        List<Wallpaper> wallpapers;
        User u = (User) request.getSession().getAttribute("current_user");
        boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
        int lastId = 0;
        if (request.getParameter("lastId") != null) {
            try {
                System.out.println(lastId);
                lastId = Integer.parseInt(request.getParameter("lastId"));
            } catch (NumberFormatException e) {
//                e.printStackTrace();
            }
        }
        if (request.getParameter("query") != null) {
            String query = request.getParameter("query");
            lastId = Integer.parseInt(request.getParameter("lastId"));
            wallpapers = TagDAO.searchTagsInUploaded(u, query, lastId, LIMIT);
        } else {
            wallpapers = WallpaperDAO.getLoadedWallpapers(u, "uploaded", lastId, LIMIT);
        }
        if (ajax) {
            Gson gson = new Gson();
            String json = gson.toJson(Wallpaper.prepareList(wallpapers));
            System.out.println(json);
            response.setContentType("application/json");
            response.getWriter().write(json);
            return;
        }
        root.put("user", u);
        root.put("wallpapers", Wallpaper.prepareList(wallpapers));
        FreemarkerConfig.process(this, root, response);
    }
}
