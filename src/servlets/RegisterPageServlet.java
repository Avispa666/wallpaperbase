package servlets;

import DB.UserDAO;
import Freemarker.FreemarkerConfig;
import antities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by avispa on 17/10/2017.
 */
@WebServlet(name = "servlets.RegisterPageServlet")
public class RegisterPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("register do post");
        if (request.getParameter("username_check") != null) {
            String username = request.getParameter("username_check");
            System.out.println("request for checking username:" + username + " handled");
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(UserDAO.isUsernameFree(username) + "");
            return;
        }
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        if (!checkInput(username, password, password2)) {
            response.sendRedirect("/register");
            return;
        }
        User user = UserDAO.registerUser(username, password);
        request.getSession().setAttribute("current_user", user);
        response.sendRedirect("/");
    }

    private boolean checkInput(String username, String password, String password2) {
        return  username != null && password != null && password2 != null
                && !Objects.equals(username, "") && !Objects.equals(password, "") && !Objects.equals(password2, "")
                && password.equals(password2) && password.length() <= 25
                && username.length() <= 20 && UserDAO.isUsernameFree(username) ; //TODO check by regex
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> root = new HashMap<>();
        FreemarkerConfig.process(this, root, response);
    }
}
