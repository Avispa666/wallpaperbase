package servlets;

import DB.CommentDAO;
import DB.TagDAO;
import DB.VoteDAO;
import DB.WallpaperDAO;
import Freemarker.FreemarkerConfig;
import antities.Comment;
import antities.User;
import antities.Wallpaper;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by avispa on 23/10/2017.
 */
@WebServlet(name = "WallpaperPageServlet")
public class WallpaperPageServlet extends HttpServlet {
    private static final String pattern = ".*\\/wallpapers\\/(\\d+)$";
    private static int wallpaperId;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Matcher m = Pattern.compile(pattern).matcher(request.getRequestURI());
        if (!m.find()) return;
        wallpaperId = Integer.parseInt(m.group(1));
        //TODO check address (404)
        User u = (User) request.getSession().getAttribute("current_user");
        System.out.println(request.getParameter("vote"));
        if (request.getParameter("comment") != null) { //adding comm    ent
            if (u == null) {
                response.setContentType("application/json");
                response.getWriter().write("");
                return;
            }
            System.out.println("userID: " + u.getId());
            String comm = request.getParameter("comment").trim();
            if (comm.equals("") || comm.length() > 300) return;
            System.out.println("len: " + comm.length());
            Comment comment = new Comment(u.getId(),
                    u.getUsername(), u.getImgUrl(),
                    wallpaperId, comm);
            comment = CommentDAO.insertComment(comment);
            if (comment == null) {
                response.sendRedirect(request.getRequestURI());
                return;
            }
            comment.setUsername(u.getUsername());
            comment.setProfileImgUrl(u.getImgUrl());
            Gson gson = new Gson();
            String json = gson.toJson(comment);
            System.out.println(json);
            response.setContentType("application/json");
            response.getWriter().write(json);
            System.out.println("response sended");
            return;
        }
        if (request.getParameter("delete_comment") != null && u != null) {
            int commentId = Integer.parseInt(request.getParameter("id"));
            System.out.println("delete comment " + commentId);
            CommentDAO.deleteComment(commentId, u);
            response.setContentType("text");
            response.getWriter().write("success");
            return;
        }
        if (request.getParameter("vote") != null) {
            System.out.println("if clause: " + request.getParameter("vote"));
            Cookie[] cookies = request.getCookies();
            boolean cookiePrepared = false;
            for (Cookie c : cookies) {
                if (!"vote".equals(c.getName())) continue;
                Matcher m1 = Pattern.compile("\\D"+ wallpaperId + "\\D").matcher(c.getValue());
                System.out.println("cookie_value: " + c.getValue());
                if (m1.find()) return; //already voted
                c.setValue(c.getValue() + wallpaperId + "-");
                c.setMaxAge(60 * 60 * 24 * 365); //1 year
                response.addCookie(c);
                cookiePrepared = true;
                break;
            }
            if (!cookiePrepared) {
                Cookie c = new Cookie("vote", "-" + wallpaperId + "-");
                c.setMaxAge(60 * 60 * 24 * 365); //1 year
                response.addCookie(c);
            }
            switch (request.getParameter("vote")) {
                case "up":
                    VoteDAO.vote(wallpaperId, u, "up");
                    break;
                case "down":
                    VoteDAO.vote(wallpaperId, u, "down");
                    break;
                default:
                    return;
            }
            System.out.println("gonna send response");
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("success");
            System.out.println("response sended");
            return;
        }
//        Comment comment = new Comment(u.getId(),
//                u.getUsername(),
//                Integer.parseInt(m.group(1)), request.getParameter("commentary"));
//        DBHelper.insertComment(comment);
//        response.sendRedirect(request.getRequestURI());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Wallpaper w;
        Matcher m = Pattern.compile(pattern).matcher(request.getRequestURI());
        if (m.find()) {
            System.out.println("id:" + wallpaperId);
            try {
                wallpaperId = Integer.parseInt(m.group(1));
                w = WallpaperDAO.getWallpaper(wallpaperId);
            } catch (Exception e) {
                response.sendRedirect("/");
                return;
            }
        }  else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        w.fetchComments();
        w.fetchTags();
        Map<String, Object> root = new HashMap<>();
        root.put("wallpaper", w);
        root.put("tags", TagDAO.getTagsForWallpaper(w));
        root.put("user", request.getSession().getAttribute("current_user"));
        FreemarkerConfig.process(this, root, response);
    }
}
