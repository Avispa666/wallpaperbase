package DB;

import antities.Category;
import antities.UnmoderatedWallpaper;
import antities.User;
import antities.Wallpaper;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;

/**
 * Created by avispa on 15/11/2017.
 */
public class WallpaperDAO {
    private static final String PATTERN = "[.]\\w+$";

    private static BufferedImage createThumb(BufferedImage in, int w, int h) {
        // scale w, h to keep aspect constant
        double outputAspect = 1.0 * w / h;
        double inputAspect = 1.0 * in.getWidth() / in.getHeight();
        if (outputAspect < inputAspect) {
            // width is limiting factor; adjust height to keep aspect
            h = (int) (w / inputAspect);
        } else {
            // height is limiting factor; adjust width to keep aspect
            w = (int) (h * inputAspect);
        }
        BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = bi.createGraphics();
        g2.setRenderingHint(
                RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(in, 0, 0, w, h, null);
        g2.dispose();
        return bi;
    }

    public static Wallpaper getWallpaper(int id) {
        Wallpaper wp = null;
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM \"wallpapers\" WHERE id = ?"
            );
            PreparedStatement ps1 = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM \"categories\" WHERE id = ?"
            );
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                ps1.setInt(1, rs.getInt("category_id"));
                ResultSet rs1 = ps1.executeQuery();
                if (!rs1.next()) throw new SQLException("wallpaper's category id not match with any category");
                wp = new Wallpaper(rs.getInt("id"),
                        new Category(rs.getInt("category_id"), rs1.getString("name")), rs.getString("picture"));
            } else {
                throw new NullPointerException("can't find requested wallpaper");
            }
            ps.close();
            ps1.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (wp == null) throw new NullPointerException("wp is null in dbhelper, id=" + id);
        return wp;
    }

    public static java.util.List<Wallpaper> getWallpapers(int fromId, int limit) {
        java.util.List<Wallpaper> wallpapers = new ArrayList<>(limit);
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT w.id AS wid, w.picture AS wpic, c.id AS cid, c.name AS cn " +
                            "FROM wallpapers AS w JOIN categories AS c " +
                            "ON (w.category_id = c.id) WHERE w.id > ? LIMIT ?"
            );
            ps.setInt(1, fromId);
            ps.setInt(2, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                wallpapers.add(new Wallpaper(rs.getInt("wid"),
                        new Category(rs.getInt("cid"), rs.getString("cn")), rs.getString("wpic")));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return wallpapers;
    }

    public static java.util.List<Wallpaper> getWallpapersByCategory(String categoryName, int lastId, int limit) {
        java.util.List<Wallpaper> wallpapers = new ArrayList<>(limit);
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT w.id AS wid, w.picture AS wpic, c.id AS cid, c.name AS cn " +
                            "FROM wallpapers AS w JOIN categories AS c " +
                            "ON (w.category_id = c.id) WHERE c.name = ? and w.id > ? LIMIT ?"
            );
            ps.setString(1, categoryName);
            ps.setInt(2, lastId);
            ps.setInt(3, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                wallpapers.add(new Wallpaper(rs.getInt("wid"),
                        new Category(rs.getInt("cid"), rs.getString("cn")), rs.getString("wpic")));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return wallpapers;
    }

    public static java.util.List<Wallpaper> getLoadedWallpapers(User user, String action, int lastId, int limit) {
        switch (action) {
            case "uploaded":
                action = "uploads";
                break;
            case "downloaded":
                action = "downloads";
                break;
            default:
                throw new InputMismatchException("undefined action: " + action);
        }
        java.util.List<Wallpaper> wallpapers = new ArrayList<>(limit);
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT categories.name AS c_name, categories.id AS c_id, " +
                            "wp.wallpaper_id AS w_id, wp.picture AS w_pic FROM categories JOIN (" +
                            "SELECT * FROM wallpapers AS wps JOIN " + action + " AS x ON x.wallpaper_id = wps.id " +
                            "WHERE x.user_id = ?) " +
                            "AS wp ON wp.category_id = categories.id WHERE wp.wallpaper_id > ? LIMIT ?"
            );
            ps.setInt(1, user.getId());
            ps.setInt(2, lastId);
            ps.setInt(3, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                wallpapers.add(new Wallpaper(rs.getInt("w_id"),
                        new Category(rs.getInt("c_id"), rs.getString("c_name")), rs.getString("w_pic")));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return wallpapers;
    }

    public static void addUnmoderated(int userId, String category, String tags, String fileName) {
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "INSERT INTO \"unmoderated\" (user_id, category, tags, filename) values (?,?,?,?)"
            );
            ps.setInt(1, userId);
            ps.setString(2, category);
            ps.setString(3, tags);
            ps.setString(4, fileName);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static java.util.List<UnmoderatedWallpaper> getUnmoderatedWallpapers() {
        java.util.List<UnmoderatedWallpaper> wallpapers = new ArrayList<>();
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM unmoderated"
            );
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                wallpapers.add(new UnmoderatedWallpaper(rs.getInt("id"), rs.getString("filename"), rs.getString("category"),
                        rs.getString("tags"), rs.getInt("user_id")));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return wallpapers;
    }

    public static void declineWallpaper(int id) {
        try {
            PreparedStatement ps0 = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM unmoderated WHERE id = ?"
            );
            ps0.setInt(1, id);
            ResultSet rs0 = ps0.executeQuery();
            if (rs0.next()) {
                Path file = Paths.get(DBHelper.WAR_PATH + "\\wp_unmoderated\\" + rs0.getString("filename"));
                Files.delete(file);
            }
            ps0.close();
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "DELETE FROM unmoderated WHERE id = ?"
            );
            ps.setInt(1, id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void acceptWallpaper(int id, String tags, String cat) {
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM unmoderated WHERE id = ?"
            );
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String t1 = PATTERN;
                Path file = Paths.get(DBHelper.WAR_PATH + "\\wp_unmoderated\\" + rs.getString("filename"));
                //TODO parse category, tags and insert into wallpapers
                PreparedStatement ps0 = DBHelper.getConnection().prepareStatement(
                        "INSERT INTO categories (name) VALUES (?) ON CONFLICT DO NOTHING"
                );
                String t = cat.trim().toLowerCase();
                ps0.setString(1, t);
                ps0.executeUpdate();
                ps0.close();
                ps0 = DBHelper.getConnection().prepareStatement(
                        "SELECT id FROM categories WHERE name = ?"
                );
                ps0.setString(1, t);
                ResultSet rs0 = ps0.executeQuery();
                if (!rs0.next()) throw new SQLException("cannot find category");
                PreparedStatement ps1 = DBHelper.getConnection().prepareStatement(
                        "INSERT INTO wallpapers (picture, category_id) " +
                                "VALUES (?, ?) " +
                                "RETURNING id"
                );
                t = rs.getString("filename");
                String ext = t.substring(t.lastIndexOf('.'));
                ps1.setString(1, ext);//set extension
                ps1.setInt(2, rs0.getInt("id"));
                ResultSet rs1 = ps1.executeQuery();
                ps1 = DBHelper.getConnection().prepareStatement(
                        "update wallpapers set picture = concat(id, picture) where picture = ?"
                );
                ps1.setString(1, ext);
                ps1.executeUpdate();
                if (!rs1.next()) throw new SQLException("cannot insert into wallpapers");
                //rename wallpaper to 'id'.'ext' and move to /wp
                BufferedImage b = createThumb(ImageIO.read(Files.newInputStream(file)),
                        256, 256);
                Path thumb = file.getParent().getParent().resolve(DBHelper.WAR_PATH + "\\thumbnails\\" + rs1.getString("id") + ext);
                Path target = file.getParent().getParent().resolve(DBHelper.WAR_PATH + "\\wp\\" + rs1.getString("id") + ext);
                ImageIO.write(b, ext.substring(1), Files.newOutputStream(thumb));
                Files.move(file, target);
                ps1.close();
                ps0.close();
                int wallpaperId = rs1.getInt("id");
                String[] tagsArr = tags.split(" ");
                for (int i = 0; i < tagsArr.length; ++i) {
                    tagsArr[i] = tagsArr[i].toLowerCase();
                    PreparedStatement pst = DBHelper.getConnection().prepareStatement(
                            "INSERT INTO tag_names(\"name\") VALUES (?) ON CONFLICT DO NOTHING;"
                    );
                    pst.setString(1, tagsArr[i]);
                    pst.executeUpdate();
                    pst.close();
                    pst = DBHelper.getConnection().prepareStatement(
                            "SELECT * FROM tag_names WHERE \"name\" = ?;"
                    );
                    pst.setString(1, tagsArr[i]);
                    ResultSet rst = pst.executeQuery();
                    if (rst.next()) {
                        int idt = rst.getInt("id");
                        pst.close();
                        pst = DBHelper.getConnection().prepareStatement(
                                "INSERT into tags(wallpaper_id, tag_id) VALUES (?,?)"
                        );
                        pst.setInt(1, wallpaperId);
                        pst.setInt(2, idt);
                        pst.executeUpdate();
                    } else {
                        throw new SQLException("cannot find tag");
                    }
                    pst.close();
                }

            }
            PreparedStatement ps1 = DBHelper.getConnection().prepareStatement(
                    "DELETE FROM unmoderated WHERE id = ?"
            );
            ps1.setInt(1, id);
            ps1.executeUpdate();
            ps1.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
