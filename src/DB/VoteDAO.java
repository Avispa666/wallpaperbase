package DB;

import antities.User;
import antities.VotesPair;
import antities.Wallpaper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by avispa on 21/11/2017.
 */
public class VoteDAO {

    public static void vote(int wallpaperId, User user, String vote) {
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "INSERT INTO \"votes\" (wallpaper_id, user_id, vote) values (?,?,?::\"vote\")"
            );
            ps.setInt(1, wallpaperId);
            if (user == null) ps.setNull(2, java.sql.Types.INTEGER);
            else ps.setInt(2, user.getId());
            ps.setString(3, vote); // 'up' or 'down'
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static VotesPair getVotesForWallpaper(Wallpaper wallpaper) {
        if (wallpaper == null) throw new NullPointerException("wallpaper is null, unable to get votees");
        VotesPair vp = null;
        try {
            PreparedStatement up = DBHelper.getConnection().prepareStatement(
                    "SELECT COUNT(vote) FROM votes WHERE wallpaper_id = ? AND vote = 'up'"
            );
            PreparedStatement down = DBHelper.getConnection().prepareStatement(
                    "SELECT COUNT(vote) FROM votes WHERE wallpaper_id = ? AND vote = 'down'"
            );
            up.setInt(1, wallpaper.getId());
            down.setInt(1, wallpaper.getId());
            ResultSet upRs = up.executeQuery();
            ResultSet downRs = down.executeQuery();
            int upCount = 0, downCount = 0;
            if (upRs.next()) {
                upCount = upRs.getInt("count");
            }
            if (downRs.next()) {
                downCount = downRs.getInt("count");
            }
            vp = new VotesPair(upCount, downCount);
            upRs.close();
            downRs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (vp == null) throw new NullPointerException("VotesPair is null after query");
        return vp;
    }
}
