package DB;

import antities.Category;
import antities.Tag;
import antities.User;
import antities.Wallpaper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by avispa on 21/11/2017.
 */
public class TagDAO {

    public static List<Tag> getTagsForWallpaper(Wallpaper wallpaper) {
        List<Tag> tags = new ArrayList<>();
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM tags JOIN tag_names ON tags.tag_id = tag_names.id WHERE wallpaper_id = ?"
            );
            ps.setInt(1, wallpaper.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                tags.add(new Tag(rs.getInt("tag_id"), rs.getString("name")));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tags;
    }

    //    public static void main(String[] args) {
//        Path dir = Paths.get(WAR_PATH + "\\wp");
//        Path thumbDir = Paths.get(WAR_PATH + "\\thumbnails");
//        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
//            for (Path file: stream) {
//                BufferedImage b = createThumb(ImageIO.read(Files.newInputStream(file)),
//                        256, 256);
//                Path thumb = thumbDir.resolve(file.getFileName());
//                String ext = file.getFileName().toString();
//                ext = ext.substring(ext.lastIndexOf('.') + 1);
//                ImageIO.write(b, ext, Files.newOutputStream(thumb));
//            }
//        } catch (IOException | DirectoryIteratorException x) {
//            // IOException can never be thrown by the iteration.
//            // In this snippet, it can only be thrown by newDirectoryStream.
//            System.err.println(x);
//        }
//    }
    public static List<Wallpaper> searchTags(String _tags, int lastId, int limit) {
        if ("".equals(_tags) || _tags == null) return WallpaperDAO.getWallpapers(lastId, limit);
        List<Wallpaper> res = new ArrayList<>(limit);
        String[] tagsArr = _tags.split(" ");
        try {
            String cats = "where ";
            for (int i = 0; i < tagsArr.length; ++i) {
                if (i != tagsArr.length - 1) cats += "\"name\" = ? OR ";
                else cats += "\"name\" = ?";
            }
            //check if non tag, but category
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT name FROM categories " + cats
            );
            for (int i = 0; i < tagsArr.length; ++i) {
                ps.setString(i + 1, tagsArr[i]);
            }
            ResultSet rs = ps.executeQuery();
            List<String> categories = new ArrayList<>(cats.length() / 2);
            while (rs.next()) {
                categories.add(rs.getString("name"));
            }
            cats = "where ";
            for (int i = 0; i < categories.size() && categories.size() != 0; ++i) {
                if (i != categories.size() - 1) cats += "\"name\" = ? OR ";
                else cats += "\"name\" = ?";
            }
            String tags = "where ";
            for (int i = 0; i < tagsArr.length; ++i) {
                if (i != tagsArr.length - 1) tags += "tn.\"name\" = ? OR ";
                else tags += "tn.\"name\" = ?";
            }
            if (categories.size() == 0) cats = "";
            ps = DBHelper.getConnection().prepareStatement(
                    "select w.id, picture, count(w.name) as count from (wallpapers join " +
                            "(select wallpaper_id, name from tags as t join tag_names as tn on t.tag_id = tn.id " +
                            tags + ") as t " +
                            "on wallpapers.id = t.wallpaper_id) as w join categories as c on w.category_id = c.id " +
                            cats + " group by w.id HAVING w.id > ? order by count desc"
            );
            int i = 0;
            for (; i < tagsArr.length; ++i) {
                ps.setString(i + 1, tagsArr[i]);
            }
            for (; i < categories.size(); ++i) {
                ps.setString(i + 1, categories.get(i - 1));
            }
            ps.setInt(i + 1, lastId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByWpId(rs.getInt("id"));
                res.add(new Wallpaper(rs.getInt("id"), c, rs.getString("picture")));
            }
            //create condition checking part for statement from categories
            //exclude categories from tags
            //build prepared statement

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static List<Wallpaper> searchTagsInCategory(String category, String _tags, int lastId, int limit) {
        if ("".equals(_tags) || _tags == null) return WallpaperDAO.getWallpapersByCategory(category, lastId, limit);
        limit = 100; //sry for this :(
        List<Wallpaper> res = new ArrayList<>(limit);
        String[] tagsArr = _tags.split(" ");
        try {
            String tags = "where ";
            for (int i = 0; i < tagsArr.length; ++i) {
                if (i != tagsArr.length - 1) tags += "tn.\"name\" = ? OR ";
                else tags += "tn.\"name\" = ?";
            }
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "select w.id, picture, count(w.name) as count from (wallpapers join " +
                            "(select wallpaper_id, name from tags as t join tag_names as tn on t.tag_id = tn.id " +
                            tags + ") as t " +
                            "on wallpapers.id = t.wallpaper_id) as w join categories as c on w.category_id = c.id " +
                            "WHERE c.name = ? group by w.id order by count desc"
            );
            int i = 0;
            for (; i < tagsArr.length; ++i) {
                ps.setString(i + 1, tagsArr[i]);
            }
            ps.setString(i + 1, category);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByWpId(rs.getInt("id"));
                res.add(new Wallpaper(rs.getInt("id"), c, rs.getString("picture")));
            }
            //create condition checking part for statement from categories
            //exclude categories from tags
            //build prepared statement

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static List<Wallpaper> searchTagsInUploaded(User u, String _tags, int lastId, int limit) {
        if ("".equals(_tags) || _tags == null) return WallpaperDAO.getLoadedWallpapers(u, "uploaded", lastId, limit);
        List<Wallpaper> res = new ArrayList<>(limit);
        String[] tagsArr = _tags.split(" ");
        try {
            String cats = "where ";
            for (int i = 0; i < tagsArr.length; ++i) {
                if (i != tagsArr.length - 1) cats += "\"name\" = ? OR ";
                else cats += "\"name\" = ?";
            }
            //check if non tag, but category
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT name FROM categories " + cats
            );
            for (int i = 0; i < tagsArr.length; ++i) {
                ps.setString(i + 1, tagsArr[i]);
            }
            ResultSet rs = ps.executeQuery();
            List<String> categories = new ArrayList<>(cats.length() / 2);
            while (rs.next()) {
                categories.add(rs.getString("name"));
            }
            cats = "where ";
            for (int i = 0; i < categories.size() && categories.size() != 0; ++i) {
                if (i != categories.size() - 1) cats += "\"name\" = ? OR ";
                else cats += "\"name\" = ?";
            }
            String tags = "where ";
            for (int i = 0; i < tagsArr.length; ++i) {
                if (i != tagsArr.length - 1) tags += "tn.\"name\" = ? OR ";
                else tags += "tn.\"name\" = ?";
            }
            if (categories.size() == 0) cats = "";
            ps = DBHelper.getConnection().prepareStatement(
                    "select w.id, max(picture) as picture, count(w.name) as count from (" +
                            "(select w.id, w.picture, w.category_id from wallpapers as w join uploads as u on w.id = u.wallpaper_id " +
                            "where u.user_id = ?) as wallpapers join " +
                            "(select wallpaper_id, name from tags as t join tag_names as tn on t.tag_id = tn.id " +
                            tags + ") as t " +
                            "on wallpapers.id = t.wallpaper_id) as w join categories as c on w.category_id = c.id " +
                            cats + " group by w.id HAVING w.id > ? order by count desc"
            );
            ps.setInt(1, u.getId());
            int i = 1;
            for (; i - 1 < tagsArr.length; ++i) {
                ps.setString(i + 1, tagsArr[i - 1]);
            }
            for (; i - 2 < categories.size(); ++i) {
                ps.setString(i + 1, categories.get(i - 2));
            }
            ps.setInt(i + 1, lastId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category c = CategoryDAO.getCategoryByWpId(rs.getInt("id"));
                res.add(new Wallpaper(rs.getInt("id"), c, rs.getString("picture")));
            }
            //create condition checking part for statement from categories
            //exclude categories from tags
            //build prepared statement

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
}
