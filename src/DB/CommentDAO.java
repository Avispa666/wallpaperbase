package DB;

import antities.Comment;
import antities.User;
import antities.Wallpaper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by avispa on 15/11/2017.
 */
public class CommentDAO {

    public static Comment insertComment(Comment comment) {
        System.out.println("comment inserted");
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "INSERT INTO \"comments\" (wallpaper_id, user_id, text) values (?,?,?) RETURNING *"
            );
            ps.setInt(1, comment.getWallpaperId());
            ps.setInt(2, comment.getUserId());
            ps.setString(3, comment.getContent());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Comment(rs.getInt("id"), rs.getInt("user_id"), null, rs.getInt("wallpaper_id"),
                        rs.getString("text"), rs.getObject("create_time", OffsetDateTime.class), null);
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Comment> getCommentsForWallpaper(Wallpaper wallpaper) {
        if (wallpaper == null) throw new NullPointerException("wallpaper is null, unable to get comments");
        ArrayList<Comment> lst = new ArrayList<>(15);
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM comments JOIN users ON comments.user_id = users.id " +
                            "WHERE wallpaper_id = ? ORDER BY create_time"
            );
            ps.setInt(1, wallpaper.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                lst.add(new Comment(rs.getInt("id"), rs.getInt("user_id"), rs.getString("username"),
                        rs.getInt("wallpaper_id"), rs.getString("text"), rs.getObject("create_time", OffsetDateTime.class),
                        rs.getString("profile_img")));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static void deleteComment(int commentId, User u) {
        String s;
        if (u.isAdmin()) s = "DELETE FROM \"comments\" WHERE id = ?";
        else s = "DELETE FROM \"comments\" WHERE id = ? AND user_id = ?";
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(s);
            ps.setInt(1, commentId);
            if (!u.isAdmin()) ps.setInt(2, u.getId());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
