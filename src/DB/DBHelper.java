package DB;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by avispa on 17/10/2017.
 */
public class DBHelper {
    public static final String WAR_PATH = "D:\\Workspace\\2k1sem_work\\WallpaperBase" +
            "\\out\\artifacts\\WallpaperBase_war_exploded";
    private static final String url = "jdbc:postgresql://localhost:5432/WallpapersBase";
    private static final String user = "postgres";
    private static final String password = "postgresql";

    // JDBC variables for opening and managing connection
    private static Connection con;

    public static Connection getConnection() {
        if (con != null) return con;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }

}
