package DB;

import antities.Category;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by avispa on 21/11/2017.
 */
public class CategoryDAO {

    public static Category getCategoryByWpId(int id) {
        Category res = null;
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT c.id, c.name FROM wallpapers AS wp " +
                            "JOIN categories AS c ON wp.category_id = c.id " +
                            "WHERE wp.id = ?"
            );
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                res = new Category(rs.getInt("id"), rs.getString("name"));
            } else {
                throw new SQLException("cannot find category for wallpaper");
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static List<Category> getCategories(int lastId, int limit) {
        List<Category> categories = new ArrayList<>(limit);
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM \"categories\" WHERE id > ? LIMIT ?"
            );
            ps.setInt(1, lastId);
            ps.setInt(2, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PreparedStatement ps1 = DBHelper.getConnection().prepareStatement(
                        "SELECT * FROM wallpapers WHERE category_id = ? ORDER BY RANDOM() LIMIT 1"
                );
                ps1.setInt(1, rs.getInt("id"));
                ResultSet rs1 = ps1.executeQuery();
                if (rs1.next())
                    categories.add(new Category(rs.getInt("id"), rs.getString("name"), rs1.getString("picture")));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }

    public static List<Category> searchInCategories(String query, int lastId, int limit) {
        List<Category> categories = new ArrayList<>(limit);
        if ("".equals(query)) return getCategories(lastId, limit);
        String[] queryCategories = query.split(" ");
        try {
            String cats = "AND (";
            for (int i = 0; i < queryCategories.length; ++i) {
                if (i != queryCategories.length - 1) cats += "\"name\" = ? OR ";
                else cats += "\"name\" = ?)";
            }
            System.out.println(cats);
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM \"categories\" WHERE id > ? " + cats + " LIMIT ?"
            );
            ps.setInt(1, lastId);
            for (int i = 0; i < queryCategories.length; ++i) ps.setString(i + 2, queryCategories[i].toLowerCase());
            ps.setInt(queryCategories.length + 2, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PreparedStatement ps1 = DBHelper.getConnection().prepareStatement(
                        "SELECT * FROM wallpapers WHERE category_id = ? ORDER BY RANDOM() LIMIT 1"
                );
                ps1.setInt(1, rs.getInt("id"));
                ResultSet rs1 = ps1.executeQuery();
                if (rs1.next())
                    categories.add(new Category(rs.getInt("id"), rs.getString("name"), rs1.getString("picture")));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }
}
