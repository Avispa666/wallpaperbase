package DB;

import antities.Passwords;
import antities.User;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.UUID;

/**
 * Created by avispa on 15/11/2017.
 */
public class UserDAO {

    private static boolean idIsFree(String id) {
        boolean result = true;
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM \"tokens\" WHERE \"uuid\" = ?"
            );
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.isBeforeFirst()) result = false;
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void changeUsername(String old, String neww) {
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "UPDATE \"users\" SET \"username\"=? WHERE \"username\"=?"
            );
            ps.setString(1, neww);
            ps.setString(2, old);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void changePassword(String username, String old, String neww) {
        System.out.println("password changed");
        try {
            PreparedStatement ps0 = DBHelper.getConnection().prepareStatement(
                    "SELECT salt, password FROM \"users\" WHERE \"username\" = ?"
            );
            ps0.setString(1, username);
            ResultSet rs = ps0.executeQuery();
            if (rs.next()) {
                if (Passwords.isExpectedPassword(old.toCharArray(), rs.getBytes("salt"),
                        rs.getBytes("password"))) {
                    PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                            "UPDATE \"users\" SET \"password\" = ?, \"salt\" = ?  WHERE \"username\" = ?"
                    );
                    byte[] salt = Passwords.getNextSalt();
                    byte[] password = Passwords.hash(neww.toCharArray(), salt);
                    ps.setBytes(1, password);
                    ps.setBytes(2, salt);
                    ps.setString(3, username);
                    ps.executeUpdate();
                    ps.close();
                }
            }
            ps0.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean isUsernameFree(String username) {
        System.out.println("isUsernameFree invoked");
        boolean result = true;
        if (username.length() > 20) return false;
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM \"users\" WHERE \"username\" = ?"
            );
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) result = false;
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("DBHelper: isFree:" + result);
        return result;
    }

    public static User login(String username, String password) {
        User result = null;
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM \"users\" WHERE \"username\" = ? LIMIT 1"
            );
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                boolean auth =
                        Passwords.isExpectedPassword(password.toCharArray(), rs.getBytes("salt"),
                                rs.getBytes("password"));
                System.out.println("auth: " + auth);
                if (auth) {
                    result = new User(rs.getInt("id"),
                            rs.getString("username"), rs.getString("profile_img"));
                    byte[] salt = Passwords.getNextSalt();
                    byte[] hash = Passwords.hash(password.toCharArray(), salt);
                    PreparedStatement ps1 = DBHelper.getConnection().prepareStatement(
                            "UPDATE \"users\" SET (\"password\",\"salt\") = (?, ?) WHERE \"username\" = ?"
                    );
                    ps1.setBytes(1, hash);
                    ps1.setBytes(2, salt);
                    ps1.setString(3, rs.getString("username"));
                    ps1.executeUpdate();
                    ps1.close();
                }
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static User registerUser(String username, String password) {
        User u = null;
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "INSERT INTO \"users\"(\"username\", \"password\", \"salt\", profile_img) VALUES (?,?,?,?)"
            );
            ps.setString(1, username);
            byte[] salt = Passwords.getNextSalt();
            byte[] passwordHash = Passwords.hash(password.toCharArray(), salt);
            ps.setBytes(2, passwordHash);
            ps.setBytes(3, salt);
            ps.setString(4, "default.png");
            ps.executeUpdate();
            u = login(username, password);
            if (u == null) throw new NullPointerException("user is null in register method!");
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return u;
    }

    public static String rememberMe(String username, String password) {
        User u = login(username, password);
        String id, token, t = null;
        byte[] tokenHash = null;
        do {
            id = UUID.randomUUID().toString();
            token = UUID.randomUUID().toString();
            try {
                tokenHash = MessageDigest.getInstance("SHA-256")
                        .digest(token.getBytes(StandardCharsets.UTF_8));
                t = id + "#" + token;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        } while (!idIsFree(id));
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "INSERT INTO \"tokens\" (uuid, token, user_id) VALUES (?,?,?)"
            );
            ps.setString(1, id);
            ps.setBytes(2, tokenHash);
            ps.setInt(3, u.getId());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return t;
    }

    public static Object[] loginByCookie(String value) {
        Object[] result = null;
        String id = null;
        byte[] tokenHash = null;
        try {
            String[] s = value.split("#");
            id = s[0]; //TODO use salt
            tokenHash = MessageDigest.getInstance("SHA-256").digest(s[1].getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        User u = null;
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT user_id, token FROM tokens WHERE uuid = ?"
            );
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (Arrays.equals(rs.getBytes("token"), tokenHash)) {
                    System.out.println("token is right!");
                    PreparedStatement ps1 = DBHelper.getConnection().prepareStatement(
                            "SELECT * FROM users WHERE id = ?"
                    );
                    ps1.setInt(1, rs.getInt("user_id"));
                    ResultSet rs1 = ps1.executeQuery();
                    if (rs1.next()) {
                        u = new User(rs1.getInt("id"),
                                rs1.getString("username"), rs1.getString("profile_img"));
                        PreparedStatement ps2 = DBHelper.getConnection().prepareStatement(
                                "UPDATE tokens SET token = ? WHERE token = ?"
                        );
                        String newToken = null;
                        byte[] newTokenHash = null;
                        try {
                            newToken = UUID.randomUUID().toString();
                            newTokenHash = MessageDigest.getInstance("SHA-256")
                                    .digest(newToken.getBytes(StandardCharsets.UTF_8));
                            newToken = URLEncoder.encode(id + "#" + newToken, "UTF-8");
                        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        ps2.setBytes(1, newTokenHash);
                        ps2.setBytes(2, tokenHash);
                        ps2.executeUpdate();
                        result = new Object[]{u, newToken};
                        ps2.close();
                    }
                    ps1.close();
                } else {
                    System.out.println("I'm not a xaker, WTFFF???");
                    //TODO delete hacked record
                } //xaker

            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void deleteRememberMe(String value) {
        String id = value.split("#")[0];
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "DELETE FROM \"tokens\" WHERE \"uuid\" = ?"
            );
            ps.setString(1, id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkIfAdmin(int id) {
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "SELECT * FROM admins WHERE id = ?"
            );
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) return true;
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void changeProfileImg(User user, String ext) {
        try {
            PreparedStatement ps = DBHelper.getConnection().prepareStatement(
                    "UPDATE \"users\" SET \"profile_img\"=? WHERE \"id\"=?"
            );
            ps.setString(1, user.getId() + "." + ext);
            ps.setInt(2, user.getId());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
