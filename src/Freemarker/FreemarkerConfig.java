package Freemarker;

import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by avispa on 19/10/2017.
 */
public class FreemarkerConfig {
    private static Configuration configuration;

    public static Configuration get(ServletContext sc) {
        if (configuration == null) {
            configuration = new Configuration(Configuration.VERSION_2_3_26);
            try {
                configuration.setServletContextForTemplateLoading(
                        sc,
                        "/WEB-INF/templates"
                );
                configuration.setDefaultEncoding("utf-8");
                configuration.setTemplateExceptionHandler(
                        TemplateExceptionHandler.RETHROW_HANDLER //TODO change exception handler
                );
                configuration.setLogTemplateExceptions(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return configuration;
    }

    public static void process(HttpServlet s, Object model, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        try {
            get(s.getServletContext()).getTemplate(
                    s.getServletName().replace("Servlet", ".ftlh")).process(model,
                    response.getWriter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
