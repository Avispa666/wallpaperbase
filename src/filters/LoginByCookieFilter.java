package filters;

import DB.UserDAO;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * Created by avispa on 21/10/2017.
 */
@WebFilter(filterName = "LoginByCookieFilter")
public class LoginByCookieFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        if (((HttpServletRequest) req).getSession().getAttribute("current_user") == null) {
            Cookie[] cookies = ((HttpServletRequest) req).getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (!cookie.getName().equals("remember_me")) continue;
                    Object[] o = UserDAO.loginByCookie(URLDecoder.decode(cookie.getValue(), "UTF-8"));
                    if (o != null) {
                        ((HttpServletRequest) req).getSession().setAttribute("current_user", o[0]); //login
                        cookie.setValue((String) o[1]);
                        cookie.setMaxAge(60 * 60 * 24 * 365); //1 year
                        ((HttpServletResponse) resp).addCookie(cookie);
                        break;
                    }
                }
            }
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
