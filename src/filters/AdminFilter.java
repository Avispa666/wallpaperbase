package filters;

import antities.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by avispa on 19/11/2017.
 */
@WebFilter(filterName = "AdminFilter")
public class AdminFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest r = (HttpServletRequest) req;
        User u = (User) r.getSession().getAttribute("current_user");
        if (u != null && u.isAdmin()) {
            chain.doFilter(req, resp);
        }
        else {
            ((HttpServletResponse)resp).sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
