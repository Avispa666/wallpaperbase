package antities;

import DB.UserDAO;

/**
 * Created by avispa on 20/10/2017.
 */
public class User {
    private int id;
    private boolean admin;
    private String username;
    private String imgUrl;

    public User(int id, String username, String imgUrl) {
        this.id = id;
        this.admin = checkIfAdmin();
        this.username = username;
        this.imgUrl = "/profile_img/" + imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getId() {
        return id;
    }

    private boolean checkIfAdmin() {
        return UserDAO.checkIfAdmin(id);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public boolean isAdmin() {
        return admin;
    }

}
