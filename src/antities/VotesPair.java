package antities;

/**
 * Created by avispa on 26/10/2017.
 */
public class VotesPair {
    private int upvotes;
    private int downvotes;

    public VotesPair(int upvotes, int downvotes) {
        this.upvotes = upvotes;
        this.downvotes = downvotes;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public int getDownvotes() {
        return downvotes;
    }
}
