package antities;

import DB.CommentDAO;
import DB.TagDAO;
import DB.VoteDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by avispa on 23/10/2017.
 */
public class Wallpaper {
    protected int id;
    private int upvotes;
    private int downvotes;
    protected String url;
    protected String href;
    private String thumbnail;
    private Category category;
    private List<Comment> comments;
    private List<Tag> tags;

    public Wallpaper(int id, Category category, String filename) {
        this.id = id;
        this.url = "/wp/" + filename;
        this.thumbnail = "/thumbnails/" + filename;
        this.href = "/wallpapers/" + id;
        VotesPair vp = VoteDAO.getVotesForWallpaper(this);
        this.upvotes = vp.getUpvotes();
        this.downvotes = vp.getDownvotes();
        this.category = category;
    }

    protected Wallpaper(int id, String url) {
        this.url = url;
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public String getHref() {
        return href;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public int getDownvotes() {
        return downvotes;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void fetchComments() {
        comments = CommentDAO.getCommentsForWallpaper(this);
    }

    public void fetchTags() {
        tags = TagDAO.getTagsForWallpaper(this);
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public static List<List<Wallpaper>> prepareList(List<Wallpaper> wallpapers) {
        List<List<Wallpaper>> res = new ArrayList<>(wallpapers.size() / 4 + 1);
        for (int i = 0; i < wallpapers.size(); ) {
            List<Wallpaper> t = new ArrayList<>(4);
            for (int j = 0; j < 4 && i < wallpapers.size(); ++j) t.add(wallpapers.get(i++));
            res.add(t);
        }
        return res;
    }
}
