package antities;

/**
 * Created by avispa on 9/11/2017.
 */
public class UnmoderatedWallpaper extends Wallpaper {
    private String categoryString;
    private String tagsString;
    private int userId;

    public UnmoderatedWallpaper(int id, String url, String category, String tags, int userId) {
        super(id, "/wp_unmoderated/" + url);
        this.categoryString = category;
        this.tagsString = tags;
        this.userId = userId;
    }

    public String getCategoryString() {
        return categoryString;
    }

    public String getTagsString() {
        return tagsString;
    }

    public int getUserId() {
        return userId;
    }
}
