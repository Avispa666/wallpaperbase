package antities;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Created by avispa on 24/10/2017.
 */
public class Comment {
    private int id;
    private int userId;
    private String profileImgUrl;
    private int wallpaperId;
    private String content;
    private String username;
    private String time;
    private static final String PATTERN = "d MMMM YYYY 'at' H:mm:ss";

    public Comment(int id, int userId, String username, int wallpaperId, String content, OffsetDateTime odt, String profileImgUrl) {
        this.id = id;
        this.userId = userId;
        this.username = username;
        this.wallpaperId = wallpaperId;
        this.content = content;
        this.profileImgUrl = "/profile_img/" + profileImgUrl;
        odt = odt.withOffsetSameInstant(ZoneOffset.ofHours(3));
        this.time = odt.format(DateTimeFormatter.ofPattern(PATTERN));
    }

    public Comment(int userId, String username, String imgUrl, int wallpaperId, String content) {
        this.userId = userId;
        this.username = username;
        this.profileImgUrl = imgUrl;
        this.wallpaperId = wallpaperId;
        this.content = content;
        this.time = OffsetDateTime.now().withOffsetSameInstant(ZoneOffset.ofHours(3))
                .format(DateTimeFormatter.ofPattern(PATTERN));

    }

    public int getId() {
        return id;
    }

    public void setProfileImgUrl(String profileImgUrl) {
        this.profileImgUrl = profileImgUrl;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public int getUserId() {
        return userId;
    }

    public int getWallpaperId() {
        return wallpaperId;
    }

    public String getTime() {
        return time;
    }

    public String getProfileImgUrl() {
        return profileImgUrl;
    }

    public String getContent() {
        return content;
    }

}
