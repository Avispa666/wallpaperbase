package antities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by avispa on 26/10/2017.
 */
public class Category {
    private int id;
    private String name;
    private String href;
    private String thumbnail;

    public Category(int id, String name) {
        this.id = id;
        this.name =  com.sun.xml.internal.ws.util.StringUtils.capitalize(name);
        this.href = "/categories/" + name;
    }

    public Category(int id, String name, String imgUrl) {
        this(id, name);
        this.thumbnail = "/thumbnails/" + imgUrl;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getHref() {
        return href;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return com.sun.xml.internal.ws.util.StringUtils.capitalize(name);
    }

    public static List<List<Category>> prepareList(List<Category> categories) {
        List<List<Category>> res = new ArrayList<>(categories.size() / 4 + 1);
        for (int i = 0; i < categories.size(); ) {
            List<Category> t = new ArrayList<>(4);
            for (int j = 0; j < 4 && i < categories.size(); ++j) t.add(categories.get(i++));
            res.add(t);
        }
        return res;
    }
}
