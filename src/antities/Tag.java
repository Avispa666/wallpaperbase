package antities;

/**
 * Created by avispa on 10/11/2017.
 */
public class Tag {
    private int id;
    private String name;

    public Tag(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return com.sun.xml.internal.ws.util.StringUtils.capitalize(name);
    }
}
