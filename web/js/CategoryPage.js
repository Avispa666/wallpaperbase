$(document).ready(function() {
	$("#download_form").submit(function (e) {
		e.preventDefault();
		var lastId = $("#last_id").val();
		$.get(window.location.pathname, {"lastId" : lastId}, function(rawdata) {
			if(!rawdata) return;
			var data = rawdata;
			$("#last_id").val(data[data.length-1][data[data.length-1].length-1].id);
			var c = 0;
			console.log(data);
			for (var i = 0; i < data.length; ++i) {
				var code = "<div class=\"row\">";
				for(var j = 0; j < data[i].length; ++j) {
				console.log(data[i][j]);
					code +=	"<a href=" + data[i][j].href + ">" +
								"<input type=\"hidden\" id=child_" + (j+1) + " value=" + data[i][j].id + ">" +
								"<div class=\"col-sm-3\">" +
									"<div class=\"wallpaper-item well\">" +
										"<img src=" + data[i][j].thumbnail + " class=\"img-thumbnail\" alt=\"Your Wallpaper\" height=\"128\" >" +
										"<p>" +
											"<img src=\"/resources/like.png\" width=\"50\" height=\"50\"><span class=\"badge\">" + data[i][j].upvotes + "</span>" +
											"<img src=\"/resources/dislike.png\" width=\"32\" height=\"32\"><span class=\"badge\">"+ data[i][j].downvotes + "</span>" +
											"<a href=" + data[i][j].url + " download>" +
												"<img class=\"pull-right\" src=\"/resources/download.png\" width=\"40\" height=\"40\">" +
											"</a>" +
										"</p>" +
									"</div>" +
								"</div>" +
							"</a>";	
				}
				code += "</div>";
				$("#wallpaper-list").append(code);
			}
		});
	});
	var loadWallpapers = function(rawdata) {
			if(!rawdata) return;
			var data = rawdata;
			if(data.length == 0) return;
			console.log(data);
			$("#last_id").val(data[data.length-1][data[data.length-1].length-1].id);
			var max = 0;
			for (var i = 0; i < data.length; ++i) {
				var code = "<div class=\"row\">";
				for(var j = 0; j < data[i].length; ++j) {
				code +=	"<a href=" + data[i][j].href + " class=\"container_child\" target=\"_blank\">" +
								"<input type=\"hidden\" id=child_" + (j+1) + " value=" + data[i][j].id + ">" +
								"<div class=\"col-sm-3\">" +
									"<div class=\"wallpaper-item well\">" +
										"<img src=" + data[i][j].thumbnail + " class=\"img-thumbnail\" alt=\"Your Wallpaper\" height=\"128\" >" +
										"<a href=" + data[i][j].category.href + "><h3><strong>" + data[i][j].category.name + "</strong></h3></a>" +
										"<p>" +
											"<img src=\"/resources/like.png\" width=\"50\" height=\"50\"><span class=\"badge\">" + data[i][j].upvotes + "</span>" +
											"<img src=\"/resources/dislike.png\" width=\"32\" height=\"32\"><span class=\"badge\">"+ data[i][j].downvotes + "</span>" +
											"<a href=" + data[i][j].url + " download>" +
												"<img class=\"pull-right\" src=\"/resources/download.png\" width=\"40\" height=\"40\">" +
											"</a>" +
										"</p>" +
									"</div>" +
								"</div>" +
							"</a>";
							if(data[i][j].id > max) max = data[i][j].id;
				}
				$("#wallpaper-list").append(code);
			}
			$("#last_id").val(max);
	};
	$("#search_form").keypress(function(e) {
		if(e.which == 13) {
			$("#search_form").submit();
		}
	});
	$("#search_form").submit(function (e) {
		e.preventDefault();
		var query = $("#query_field").val();
		if (!query && query !== "") return;
		if (query.length > 50) {
			// url = "/";
			// window.history.pushState("","", url);
			$("#query_field").val("");
			return;
		}
		var url;
		var lastId = 0;
		$.get(window.location.pathname, {"lastId" : lastId, "query" : query}, function (rawdata) {
			if(!rawdata) return;
			$("#wallpaper-list").empty();
			loadWallpapers(rawdata);
		});
	});
});