$(document).ready(function() {
	var loadCategories = function(rawdata) {
			if(!rawdata) return;
			var data = rawdata;
			if(data.length == 0) return;
			console.log(data);
			$("#last_id").val(data[data.length-1][data[data.length-1].length-1].id);
			var max = 0;
			for (var i = 0; i < data.length; ++i) {
				var code = "<div class=\"row\">";
				for(var j = 0; j < data[i].length; ++j) {
				code +=	"<a href=" + data[i][j].href + " class=\"container_child\" target=\"_blank\">" +
								"<input type=\"hidden\" id=child_" + (j+1) +  " value=" + data[i][j].id + ">" +
								"<div class=\"col-sm-3\">" +
									"<div class=\"category-item\">" +
										"<div class=\"panel panel-default\">" +
											"<div class=\"panel-body\">" +
												"<img src=" + data[i][j].thumbnail + " class=\"img-thumbnail img-responsive\" alt=\"Your Wallpaper\" width=\"210\" height=\"210\">" +
											"</div>" +
											"<div class=\"panel-footer\">" +
												"<div class=\"text-center\"><strong> " + data[i][j].name + " </strong></div>" +
											"</div>" +
										"</div>" +
									"</div>" +
								"</div>" +
							"</a>";
							if(data[i][j].id > max) max = data[i][j].id; 
				}
				code += "</div>";
				$("#category-list").append(code);
			}
			$("#last_id").val(max);
		};
	var loadWallpapers = function(rawdata) {
			if(!rawdata) return;
			var data = rawdata;
			if(data.length == 0) return;
			console.log(data);
			$("#last_id").val(data[data.length-1][data[data.length-1].length-1].id);
			var max = 0;
			for (var i = 0; i < data.length; ++i) {
				var code = "<div class=\"row\">";
				for(var j = 0; j < data[i].length; ++j) {
				code +=	"<a href=" + data[i][j].href + " class=\"container_child\" target=\"_blank\">" +
								"<input type=\"hidden\" id=child_" + (j+1) + " value=" + data[i][j].id + ">" +
								"<div class=\"col-sm-3\">" +
									"<div class=\"wallpaper-item well\">" +
										"<img src=" + data[i][j].thumbnail + " class=\"img-thumbnail\" alt=\"Your Wallpaper\" height=\"128\" >" +
										"<a href=" + data[i][j].category.href + "><h3><strong>" + data[i][j].category.name + "</strong></h3></a>" +
										"<p>" +
											"<img src=\"/resources/like.png\" width=\"50\" height=\"50\"><span class=\"badge\">" + data[i][j].upvotes + "</span>" +
											"<img src=\"/resources/dislike.png\" width=\"32\" height=\"32\"><span class=\"badge\">"+ data[i][j].downvotes + "</span>" +
											"<a href=" + data[i][j].url + " download>" +
												"<img class=\"pull-right\" src=\"/resources/download.png\" width=\"40\" height=\"40\">" +
											"</a>" +
										"</p>" +
									"</div>" +
								"</div>" +
							"</a>";
							if(data[i][j].id > max) max = data[i][j].id;
				}
				$("#category-list").append(code);
			}
			$("#last_id").val(max);
	};
	$("#download_form").submit(function (e) {
		e.preventDefault();
		var lastId = $("#last_id").val();
		var searchAll = !document.getElementById("search_categories").checked;
		var query = $("#query_field").val();
		$.get(window.location.pathname, {"lastId" : lastId, "all" : searchAll, "query" : query}, function (rawdata) {
			if(!rawdata) return;
			$("#last_query").val(query);
			if (searchAll) loadWallpapers(rawdata);
			else loadCategories(rawdata);
		});
	});
	$("#search_form").keypress(function(e) {
		if(e.which == 13) {
			$("#search_form").submit();
		}
	});
	$("#search_form").submit(function (e) {
		e.preventDefault();
		var query = $("#query_field").val();
		if (!query && query !== "") return;
		if (query.length > 50) {
			// url = "/";
			// window.history.pushState("","", url);
			$("#query_field").val("");
			return;
		}
		var url;
		var searchAll = !document.getElementById("search_categories").checked;
		var lastId = 0;
		$.get(window.location.pathname, {"lastId" : lastId, "all" : searchAll, "query" : query}, function (rawdata) {
			if(!rawdata) return;
			// $('.container .container_child').remove();
			$("#category-list").empty();
			$("#last_query").val(query);
			if (searchAll) loadWallpapers(rawdata);
			else loadCategories(rawdata);
		});
	});
	$("#search_categories").click(function(e){
		if (document.getElementById("search_categories").checked){
			$("#find_label").text("Find category you want");
		} else {
			$("#find_label").text("Find wallpaper you want");
		}
	});
});