$(document).ready(function(){
	$(document).on("click", ".btn-moderate", function(e) {
		e.preventDefault();
		decide(this.id);
	});
});

var decide = function(id) {
	var arr = id.split("_");
	var cat = $("#category_field").val();
	var tags = $("#tags_field").val();
	$.post("/moderate", {"decision" : arr[0], "id" : arr[1],"category" : cat, "tags" : tags}, function(response) {
		if (response === "success") {
			$("#li_" + arr[1]).remove();
		}
	});
};