$(document).ready(function(){
	$("#password-form").submit(function(e){
		e.preventDefault();
		newPasswordCheck();
	});
});

function newPasswordCheck() {
    var oldPasswordField = document.getElementById('old_password');
    var newPasswordField = document.getElementById('new_password');
    var confirmNewPasswordField = document.getElementById('new_password_confirm');
    var psw1 = newPasswordField.value;
    var psw2 = confirmNewPasswordField.value;
    if (psw1 !== psw2 || psw1 === '') {
        oldPasswordField.value = '';
        newPasswordField.value = '';
        confirmNewPasswordField.value = '';
        newPasswordField.parentNode.setAttribute(
            'class',
            newPasswordField.parentNode.getAttribute('class') + ' ' + 'has-error'
        );
        confirmNewPasswordField.parentNode.setAttribute(
            'class',
            confirmNewPasswordField.parentNode.getAttribute('class') + ' ' + 'has-error'
        );
        var alert = document.createElement('div');
        alert.setAttribute('class', 'alert alert-danger alert-dismissable fade in');
        alert.innerHTML = [
            '<a class="close" data-dismiss="alert" aria-label="close">x</a>',
            '<strong>Oops!</strong> You failed password\'s confirm'
        ].join('');
        document.getElementById('password-alert').insertBefore(alert, null);
        return false;
    } else {
		$.post("/profile", {"password_set" : oldPasswordField.value + " " + psw1}, function(response){
			console.log("post sended");
			oldPasswordField.value = '';
			newPasswordField.value = '';
			confirmNewPasswordField.value = '';
		});
        return true;
    }
}