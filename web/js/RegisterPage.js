$(document).ready(function(){
	// $("#btn").click(function(e){
		// e.preventDefault();
		// if (usernameCheck($("#username_field").val()) && passwordCheck($("#password").val(), $("#password2").val())) $("#btn").submit();
	// });
	$("#username_field").blur(function(e){
		usernameCheck($("#username_field").val());
	});
	$("#password2").blur(function(e){
		passwordCheck($("#password").val(), $("#password2").val());
	});
	
});

var usernameCheck = function(username) {
    //add ajax request for checking usage of the new username
	var UsernameField = document.getElementById('username_field');
	var isUsernameFree = false; // true value for function test
	if (username.length > 20) {
		// UsernameField.value = '';
				UsernameField.parentNode.setAttribute(
						'class',
						UsernameField.parentNode.getAttribute('class') + ' ' + 'has-error'
				);

				var alert = document.createElement('div');
				alert.setAttribute('class', 'alert alert-danger alert-dismissable fade in');
				alert.innerHTML = [
				'<a class="close" data-dismiss="alert" aria-label="close">x</a>',
				'<strong>Oops!</strong> Username length shouldn\'t be more than 20'
				].join('');
				document.getElementById('username-alert').insertBefore(alert, null);
				return false;
	}
	if (username !== "") {
		$.post("/register", {"username_check" : username}, function(responseText){
			if (responseText == "false") {
				UsernameField.value = '';
				UsernameField.parentNode.setAttribute(
						'class',
						UsernameField.parentNode.getAttribute('class') + ' ' + 'has-error'
				);

				var alert = document.createElement('div');
				alert.setAttribute('class', 'alert alert-danger alert-dismissable fade in');
				alert.innerHTML = [
				'<a class="close" data-dismiss="alert" aria-label="close">x</a>',
				'<strong>Oops!</strong> This username is already in use'
				].join('');
				document.getElementById('username-alert').insertBefore(alert, null);
				return false;
			} else {
				return true;
			}
		});
	}
	$("#username_field").removeClass("alert alert-danger alert-dismissable fade in");
	return true;
}

var passwordCheck = function(password, password2) {
	if (password !== password2 || password.length < 6 || password.length > 25) {
		var PasswordField = document.getElementById('password');
		// PasswordField.value = '';
				PasswordField.parentNode.setAttribute(
						'class',
						PasswordField.parentNode.getAttribute('class') + ' ' + 'has-error'
				);
		var alert = document.createElement('div');
				alert.setAttribute('class', 'alert alert-danger alert-dismissable fade in');
				if (password !== password2)
					alert.innerHTML = [
					'<a class="close" data-dismiss="alert" aria-label="close">x</a>',
					'<strong>Oops!</strong> Passwords ain\'t match'
					].join('');
					
				else 
					alert.innerHTML = [
					'<a class="close" data-dismiss="alert" aria-label="close">x</a>',
					'<strong>Oops!</strong> Password length shouldn\'t be less than 6 or more than 25'
					].join('');
				document.getElementById('password-alert').insertBefore(alert, null);
				return true;
	}
	else {
		return true;
	}
}
// $(document).ready(function(){
	// var checkLogin
	// $("#username_field").blur(function(){
	// var username = $(this).val();
	// if (username != "") {
		// $.post("/register", {"username_check" : username}, function(responseText){
			// if (responseText == "false") {
				// $("#username_field").css("border-color","red");
				// $("#btn").prop("diabled", true);
			// } else {
				// $("#username_field").css("border-color", $("#password").css("border-color"));
				// $("#btn").prop("diabled", false);
			// }
		// });
	// } else {
		// $("#username_field").css("border-color",$("#password").css("border-color"));
		// $("#btn").prop("diabled", false);
	// }
	// });
// });