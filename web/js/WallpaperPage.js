var vote = function(vote){
	$.post(window.location.pathname, {"vote" : vote}, function(responseText){
		if (responseText == "success") {
			var t = "#" + vote + "_badge";
			// console.log($(t));
			$(t).text(Number($(t).text()) + 1);
			$("#up").off("click");
			$("#down").off("click");
		}
	});
};
$(document).ready(function(){
	$(document).on("click", "#up", function(e) {
		e.preventDefault();
		vote("up");
	});
	$(document).on("click", "#down", function(e) {
		e.preventDefault();
		vote("down");
	});
	$(document).on("click", ".glyphicon-remove", function(e) {
		e.preventDefault();
		var id = this.id;
		// console.log(this.id);
		$.post(window.location.pathname, {"delete_comment" : true, "id" : id}, function(response) {
			var t = $("#li_" + id);
			// console.log(t);
			t.remove();
		});
	});
	$("#comment_form").submit(function(e){
		e.preventDefault();
		var comment = $("#comment_field").val();
		if(comment !== "") {
			$.post(window.location.pathname, {"comment" : comment}, function(data){
			if(!data || data  == "") {
				var alert = document.createElement('div');
				alert.setAttribute('class', 'alert alert-danger alert-dismissable fade in');
				alert.innerHTML = [
				'<a class="close" data-dismiss="alert" aria-label="close">x</a>',
				'<strong>Oops!</strong> You have to log in to drop comment'
				].join('');
				document.getElementById('comment-alert').insertBefore(alert, null);
				$("#comment_field").val("");
				return;
			}
			var code = "<li class=\"list-group-item\" id=\"li_" + data.id + "\">" +
							"<div class=\"well row\">" +
								"<div class=\"col-sm-3\">" +
									"<div class=\"panel panel-default\">" +
										"<div class=\"panel-heading\">" +
											data.username +
										"</div>" +
										"<div class=\"panel-body\">" +
											"<img class=\"img-responsive\" src=" + data.profileImgUrl + ">" +
										"</div>" +
									"</div>" +
								"</div>" +
								"<div class=\"col-sm-9\">" +
									"<div class=\"panel panel-default\">" +
										"<div class=\"panel-heading\">" +
											"<div class=\"pull-left\">" +
												data.time +
											"</div>" +
											"<div class=\"pull-right\">" +
												"<span class=\"glyphicon glyphicon-remove\" id=" + data.id + "></span>" +
											"</div>" +
											"<div class=\"clearfix\"></div>" +
										"</div>" +
										"<div class=\"panel-body\">" + _.escape(data.content) + "</div>" +
									"</div>" +
								"</div>" +
							"</div>" +
						"</li>";
			$("#comment_block").append(code);
			$("#comment_field").val("");
			$("#comment_block").animate({ scrollTop: $('#comment_block').prop("scrollHeight")}, 1000);
			});
		}
	});
});