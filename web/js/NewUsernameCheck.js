$(document).ready(function(){
	$("#username_form").submit(function(e){
		e.preventDefault();
		usernameCheck($("#new_username").val())
	});
});

var usernameCheck = function(username) {
    //add ajax request for checking usage of the new username
	var newUsernameField = document.getElementById('new_username');
	var isUsernameFree = false; // true value for function test
	if (username !== "") {
		$.post("/profile", {"username_check" : username}, function(responseText){
			if (responseText == "false") {
				newUsernameField.value = '';
				newUsernameField.parentNode.setAttribute(
						'class',
						newUsernameField.parentNode.getAttribute('class') + ' ' + 'has-error'
				);

				var alert = document.createElement('div');
				alert.setAttribute('class', 'alert alert-danger alert-dismissable fade in');
				alert.innerHTML = [
				'<a class="close" data-dismiss="alert" aria-label="close">x</a>',
				'<strong>Oops!</strong> This username is already in use'
				].join('');
				document.getElementById('username-alert').insertBefore(alert, null);
				}
		});
	} 
}
